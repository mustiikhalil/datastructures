//
//  TreeNode.swift
//  BinarySearchTree
//
//  Created by Mustafa Khalil on 2/15/17.
//  Copyright © 2017 Mustafa Khalil. All rights reserved.
//

import Foundation


class TreeNode <T> {
    private var _value: T?
    private var _leftChild: TreeNode?
    private var _rightChild: TreeNode?
    var value: T{
        get{
            return _value!
        }
        set{
            _value = newValue
        }
    }
    var leftChild: TreeNode?{
        get{
            return _leftChild
        }
        set{
            _leftChild = newValue
        }
    }
    var rightChild: TreeNode?{
        get{
            return _rightChild
        }
        set{
            _rightChild = newValue
        }
    }
    
    
    
    init(value: T, left: TreeNode?, right: TreeNode?) {
        self._value = value
        self._leftChild = left
        self._rightChild = right
    }
    init(){
        self._value = nil
        self._leftChild = nil
        self._rightChild = nil
    }
}

