//
// Created by Mustafa Khalil on 9/23/17.
// Copyright (c) 2017 _MMK. All rights reserved.
//

import Foundation

class Node <T>: Equatable where T: Hashable{
    private var _data: T?
    public let index: Int
    
    init(data: T, index: Int) {
        self._data = data
        self.index = index
    }
    
    var data: T?{
        set{
           _data = data
        }
        get{
            return _data
        }
    }
    
    static func ==(lhs: Node<T>, rhs: Node<T>) -> Bool {
        guard lhs.index == rhs.index else{
            return false
        }
        guard lhs.data == rhs.data else{
            return false
        }
        return true
    }
}


